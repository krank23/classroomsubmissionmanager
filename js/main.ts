enum NamingMethod {
  STRAIGHT_EMAIL,
  EMAIL_USERNAME,
  REVERSE_EMAIL_USERNAME
}

enum DateFolderChoice {
  NEEDBE,
  ALWAYS,
  NEVER
}

var server_globals = {
  foldersTrashed: 0,
  foldersCreated: 0,
  filesMoved: 0,
  filesRenamed: 0,
  maxDepth: 16
};

var Drive; // 100% to keep the VScode typescript engine from nagging me about the Drive advanced service

/* MASTER ACTIONS - CALLED FROM OUTSIDE*/

/**
 * FLATTEN FOLDER -- moves all files from subfolders into base folder, then re-sorts and returns new sorting results
 *
 * @param {string} folderId                The id of the Google Drive base folder.
 * @param {string} dateFolderChoiceString  String determining date folder creation, NEEDBE|ALWAYS|NEVER. Passed through to sortFilesIntoFolders.
 *
 * @returns {SortingResult}                An object containing the results of the sorting operation.
 */
function flattenFolder(folderId: string, dateFolderChoiceString: string): SortingResult {
  let existingFolders: FolderReferenceMap = {};
  let existingFiles: FileReferenceMap = {};

  // Get base folder
  let baseFolder: GoogleAppsScript.Drive.Folder = DriveApp.getFolderById(folderId);

  // Read current structure into Folder/File objects
  let baseFolderObjectBefore: FolderObject = objectBuilders.buildFolderObjectFromFolder(
    baseFolder,
    existingFolders,
    existingFiles
  );

  // Get flat array of file objects
  let fileObjectsFlat: FileObject[] = objectStructure.getFileObjectsRecursively(baseFolderObjectBefore);

  // Go through all the files
  for (let i = 0; i < fileObjectsFlat.length; i++) {
    let fileObject: FileObject = fileObjectsFlat[i];

    // Get file references for current file
    let file = existingFiles[fileObject.id];

    driveManipulation.moveFile(fileObject, folderId, existingFolders, file);
  }

  // Having gone through all the files, refresh the sorting and return its results
  return sortFilesIntoFolders(folderId, true, dateFolderChoiceString);
}

/**
 * SORT FILES INTO FOLDERS -- Creates structure of FolderObjects based on the files recursively found in a google drive folder. Optinally applies the new structure to the real Drive folder as well.
 *
 * @param {string} folderId                The id of the Google Drive base folder.
 * @param {boolean} dryRun                 Whether to apply the structure to the real Drive folder (true = no changes are made).
 * @param {string}dateFolderChoiceString   String determining date folder creation, NEEDBE|ALWAYS|NEVER.
 *
 * @returns {SortingResult}                An object containing the results of the sorting operation.
 */
function sortFilesIntoFolders(folderId: string, dryRun: boolean, dateFolderChoiceString: string): SortingResult {
  let parentFolder: GoogleAppsScript.Drive.Folder = DriveApp.getFolderById(folderId);

  let dateFolderChoice: DateFolderChoice = DateFolderChoice.NEEDBE;

  switch (dateFolderChoiceString) {
    case "NEEDBE":
      dateFolderChoice = DateFolderChoice.NEEDBE;
      break;
    case "ALWAYS":
      dateFolderChoice = DateFolderChoice.ALWAYS;
      break;
    case "NEVER":
      dateFolderChoice = DateFolderChoice.NEVER;
      break;
  }

  // Create the hashmap-like for "before" folders
  let existingFolders: FolderReferenceMap = {};
  let existingFiles: FileReferenceMap = {};

  // Get the "before" structure
  let baseFolderObjectBefore = objectBuilders.buildFolderObjectFromFolder(parentFolder, existingFolders, existingFiles);

  // Create the "after" structure
  let fileObjectsFlat: FileObject[] = objectStructure.getFileObjectsRecursively(baseFolderObjectBefore); // TODO: Is this necessary?

  fileObjectsFlat = objectStructure.copyFileObjectArray(fileObjectsFlat); // TODO: Is this necessary?

  let folderObjectsStructured: FolderObject[] = objectStructure.makeFolderStructure(fileObjectsFlat, dateFolderChoice);

  let baseFolderObjectAfter = objectBuilders.buildFolderObject(parentFolder.getName());
  baseFolderObjectAfter.folders = folderObjectsStructured;
  baseFolderObjectAfter.id = parentFolder.getId();

  renameDuplicates(fileObjectsFlat);

  if (!dryRun) {
    // Do some actual structuring
    driveManipulation.applyStructureToFolder(baseFolderObjectAfter, existingFolders, existingFiles);
    // Update structure
    baseFolderObjectBefore = objectBuilders.buildFolderObjectFromFolder(parentFolder, existingFolders, existingFiles);
  }

  return {
    before: baseFolderObjectBefore,
    after: baseFolderObjectAfter,
    result: {
      foldersTrashed: server_globals.foldersTrashed,
      foldersCreated: server_globals.foldersCreated,
      filesMoved: server_globals.filesMoved,
      filesRenamed: server_globals.filesRenamed
    }
  };
}

/**
 * REMOVE EMPTY SUBFOLDERS -- Recursively look through all subfolders of the specified Drive folder, removing all that contain no files anywhere in the tree. Then re-sorts and returns new sorting results
 *
 * @param {string} folderId                The id of the Google Drive base folder.
 * @param {string} dateFolderChoiceString  String determining date folder creation, NEEDBE|ALWAYS|NEVER. Passed through to sortFilesIntoFolders.
 *
 * @returns {SortingResult}                An object containing the results of the sorting operation.
 */
function removeEmptySubfolders(folderId: string, dateFolderChoiceString: string): SortingResult {
  // Get base folder
  let baseFolder: GoogleAppsScript.Drive.Folder = DriveApp.getFolderById(folderId);

  // Trash folders
  driveManipulation.TrashFolder(baseFolder);

  // Having trashed the folders, refresh the sorting and return its results
  return sortFilesIntoFolders(folderId, true, dateFolderChoiceString);
}

/**
 * RENAME DUPLICATES -- go through an array of files, renaming those that share a file name + target Drive folder Id.
 *
 * @param {FileObject[]} fileObjects       An array of FileObjects
 */
function renameDuplicates(fileObjects: FileObject[]) {
  let numberingMap: NumberMap = {};

  fileObjects.forEach(function(fileObject: FileObject) {
    // Find all files that share the same name & folder
    let sameNamedFileObjects = fileObjects.filter(function(otherFileObject) {
      return (
        otherFileObject.targetFolderString == fileObject.targetFolderString &&
        otherFileObject.name == fileObject.name &&
        fileObject.id != otherFileObject.id
      );
    });

    // If there are more than one
    if (sameNamedFileObjects.length > 1) {
      // Rename them all
      sameNamedFileObjects.forEach(function(fileWithClones: FileObject, index: number) {
        if (!(numberingMap[fileWithClones.name] > 0)) {
          // Add timestamp to beginning of filename
          fileWithClones.name = makeTimeString(fileWithClones.date) + "_" + fileWithClones.name;

          numberingMap[fileObject.name] = 1;
        } else {
          fileWithClones.name = "(" + numberingMap[fileWithClones.name] + ") " + fileWithClones.name;
          numberingMap[fileObject.name]++;
        }
      });
    }
  });
}

/*------------------------------------------------------------------------------
    STRING MAKERS
------------------------------------------------------------------------------*/

/**
 * MAKE FOLDER NAME -- Generate a folder name based on the E-mail of the creator of a file.
 *
 * @param {FileObject} fileObject          The file object
 * @param {NamingMethod} namingMethod      The naming method -- STRAIGHT_EMAIL (straight E-mail including the 'at'), EMAIL_USERNAME (just the username), REVERSE_EMAIL_USERNAME (the username split by the period and the parts being put together in the reverse order, so firstname.lastname[at]nomail.ninja becomes lastname.firstname).
 *
 * @returns {string}                       The resulting string
 */

function makeFolderName(fileObject: FileObject, namingMethod: NamingMethod): string {
  switch (namingMethod) {
    case NamingMethod.STRAIGHT_EMAIL:
      return fileObject.creator.emailAddress;
    case NamingMethod.EMAIL_USERNAME:
      return fileObject.creator.emailAddress.split("@")[0];
    case NamingMethod.REVERSE_EMAIL_USERNAME:
      let userName: string = fileObject.creator.emailAddress.split("@")[0];
      let userNameSplit: string[] = userName.split(".").reverse();
      return userNameSplit.join(".");
  }
}

/**
 * MAKE DATE STRING -- Generates a string like 2019-03-20 from an ECMA-262 (or other Date.Parse compatible) datestring
 *
 * @param {string} dateString              The datestring
 *
 * @returns {string}                       The result
 */

function makeDateString(dateString: string): string {
  let date: Date = new Date(dateString);
  return date.toISOString().split("T")[0];
}

/**
 * MAKE TIME STRING -- Generates a string like 23.58 (hours.minutes) from an ECMA-262 (or other Date.Parse compatible) datestring. Periods instead of colons because the result is to be used in a filename.
 *
 * @param {string} dateString              The datestring
 *
 * @returns {string}                       The result
 */

function makeTimeString(dateString: string): string {
  let date: Date = new Date(dateString);
  return ("0" + date.getHours()).slice(-2) + "." + ("0" + date.getMinutes()).slice(-2);
}

/*------------------------------------------------------------------------------
BUILDING FILE & FOLDER OBJECTS
------------------------------------------------------------------------------*/

/**
 * OBJECTS STRUCTURE -- A collection of functions to create or manipulate structures of FolderObjects and FileObjects
 */

namespace objectStructure {
  /**
   * MAKE FOLDER STRUCTURE -- Generate an array of FolderObjects based on the creators of an array of FileObjects, and add each file to the right folder. Make sure files inside subfolders have unique names. Optionally add date subfolders.
   *
   * @param {FileObject[]} fileObjects     The file objects to sort into folders.
   * @param {DateFolderChoice} dateFolderChoice Determines whether to create date subfolders -- ALWAYS|NEEDBE|NEVER.
   *
   * @returns {FolderObject[]}             The resulting folder objects.
   */
  export function makeFolderStructure(fileObjects: FileObject[], dateFolderChoice: DateFolderChoice): FolderObject[] {
    // First, make base structure
    let uniqueFolders: FolderObjectMap = {};

    // Then go through all files, creating unique username-named folders as needed.

    for (let i = 0; i < fileObjects.length; i++) {
      let fileObject = fileObjects[i];

      // Prepare
      let folderName = makeFolderName(fileObject, NamingMethod.REVERSE_EMAIL_USERNAME);

      // Use previously cached hashmap of existing folders to determine if folder needs to be created.
      if (!uniqueFolders.hasOwnProperty(folderName)) {
        uniqueFolders[folderName] = objectBuilders.buildFolderObject(folderName);
      }

      // Get a reference to the new parent folder object
      let folderObject = uniqueFolders[folderName];

      // Check to see if a file by the same id has already been added to the target folder object
      let alreadyAdded: boolean =
        folderObject.files.filter(function(folderFile) {
          return folderFile.id == fileObject.id;
        }).length > 0;

      if (!alreadyAdded) {
        // Add the file to the folder object's list of files
        folderObject.files.push(fileObject);
      }

      // Set the folder object id as the target folder for the file
      fileObject.targetFolderString = folderObject.name;
    }

    // Add date subfolders to each folder
    if (dateFolderChoice != DateFolderChoice.NEVER) {
      for (let folderName in uniqueFolders) {
        let folder = uniqueFolders[folderName];

        uniqueFolders[folderName] = addDateSubfolders(folder, dateFolderChoice == DateFolderChoice.ALWAYS);
      }
    }

    // Then, create a structure of folder objects based on the above hashmap-like
    let folderObjects: FolderObject[] = [];

    for (let folderName in uniqueFolders) {
      folderObjects.push(uniqueFolders[folderName]);
    }

    // Finally, return
    return folderObjects;
  }

  /**
   * MAKE FILE OBJECTS ARRAY -- Generate and an array of FileObjects based on the files inside a Drive folder.
   *
   * @param {GoogleAppsScript.Drive.Folder} parentFolder The Drive folder whose files are to be the basis for the array.
   * @param {FolderObject} parentFolderObject The FolderObject already generated, based on the same Drive folder.
   * @param {FileReferenceMap} existingFiles A hashmap of unique files, which this function helps populate for later use.
   *
   * @returns {FileObject[]}               An array of FileOjects.
   */
  export function makeFileObjectsArray(
    parentFolder: GoogleAppsScript.Drive.Folder,
    parentFolderObject: FolderObject,
    existingFiles: FileReferenceMap
  ): FileObject[] {
    let fileList: GoogleAppsScript.Drive.FileIterator;
    fileList = parentFolder.getFiles();

    let files: FileObject[] = [];
    let file: GoogleAppsScript.Drive.File;

    // Use the iterator to walk through all files in the folder
    while (fileList.hasNext()) {
      file = fileList.next();

      // Create a new file object based on the file
      let fileObject: FileObject = objectBuilders.buildFileObjectFromFile(file);

      // Add the file to the array
      files.push(fileObject);

      // Add the file to the hashmap
      if (!existingFiles.hasOwnProperty(fileObject.id)) {
        existingFiles[fileObject.id] = file;
      }
    }
    return files;
  }

  /**
   * ADD DATE SUBFOLDERS -- Add date subfolder objects to a folder, and move its files into them. One subfolder is created per unique file creation date.
   *
   * @param {FolderObject} folderObject    The folder object to create subfolders in.
   * @param {boolean} alwaysAdd            Add subfolder objects always or when needed (=when there are multiple unique dates).
   *
   * @returns {FolderObject}               The same folder object, now possibly with date subfolders.
   */
  export function addDateSubfolders(folderObject: FolderObject, alwaysAdd: boolean): FolderObject {
    let uniqueDates: string[] = getUniqueDates(folderObject.files);

    if (uniqueDates.length > 1 || alwaysAdd) {
      let dateFolders: FolderObjectMap = {};

      // Create all the date subfolders
      for (let i = 0; i < uniqueDates.length; i++) {
        let dateFolderName = uniqueDates[i];

        if (!dateFolders.hasOwnProperty(dateFolderName)) {
          let subFolderObject: FolderObject = objectBuilders.buildFolderObject(dateFolderName);
          dateFolders[dateFolderName] = subFolderObject;
          folderObject.folders.push(subFolderObject);
        }
      }

      // Move all the file objects to the new date subfolders
      for (let i = 0; i < folderObject.files.length; i++) {
        let fileObject = folderObject.files[i];
        let dateString = makeDateString(fileObject.date);
        let dateFolder: FolderObject = dateFolders[dateString];

        // Add the file to the folder object's list of files
        dateFolder.files.push(fileObject);

        // Add the date folder's name to the target folder stringfor the file
        fileObject.targetFolderString += "\\" + dateFolder.name;
      }

      // When all file objects have been added to the new date subfolders - empty the original array
      folderObject.files = [];
    }

    return folderObject;
  }

  /**
   * GET UNIQUE DATES -- generate an array of unique datestrings from an array of FileObjects, using their creation dates.
   *
   * @param {FileObject[]} fileObjects     The array of FileObjects
   *
   * @returns {string[]}                   An array of unique datestrings
   */
  export function getUniqueDates(fileObjects: FileObject[]): string[] {
    let uniqueDates: BoolMap = {};

    // Go through all items
    for (let i = 0; i < fileObjects.length; i++) {
      let fileObject = fileObjects[i];

      let dateString = makeDateString(fileObject.date);
      uniqueDates[dateString] = true;
    }

    return Object.keys(uniqueDates);
  }

  /**
   *  GET FILE OBJECTS RECURSIVELY -- Get a flat array of all the FileObjects that are in the specified FolderObject, recursively.
   *
   * @param {FolderObject} baseFolder      The FolderObject to get files from
   * @param {number} depth                 The current recursion depth
   *
   * @returns {FileObject[]}               An array of the FileObjects inside the FolderObject and its sub-FolderObjects
   */
  export function getFileObjectsRecursively(baseFolder: FolderObject, depth: number = 0): FileObject[] {
    if (depth >= server_globals.maxDepth) {
      throw "Max subfolder depth reached";
    }

    // First, make copy of base folder's file list
    let fileObjects: FileObject[] = baseFolder.files.slice(0);

    // Then go through subfolders, getting their files as well
    for (let i = 0; i < baseFolder.folders.length; i++) {
      let folderObject: FolderObject = baseFolder.folders[i];
      let fileObjectsOfFolder: FileObject[] = getFileObjectsRecursively(folderObject, depth + 1);
      fileObjects = fileObjects.concat(fileObjectsOfFolder);
    }

    return fileObjects;
  }

  /**
   * COPY FILE OBJECT ARRAY -- make a deep copy of an array of FileObjects
   *
   * @param {FileObject[]} fileObjects     The array to make a copy of.
   *
   * @returns {FileObject[]}               The copy.
   */
  export function copyFileObjectArray(fileObjects: FileObject[]): FileObject[] {
    let newArray: FileObject[] = [];

    fileObjects.forEach(function(fileObject) {
      // Make a new FileObject, copying all primitive values from the old one, and add it to the array.
      newArray.push({
        id: fileObject.id,
        name: fileObject.name,
        currentFolderIds: fileObject.currentFolderIds,
        targetFolderString: fileObject.targetFolderString,
        date: fileObject.date,
        url: fileObject.url,
        creator: {
          displayName: fileObject.creator.displayName,
          emailAddress: fileObject.creator.emailAddress
        }
      });
    });

    return newArray;
  }
}

/**
 * OBJECT BUILDERS -- A collection of functions to create FolderObjects and FileObjects.
 */
namespace objectBuilders {
  /**
   * BUILD FILEOBJECT FROM FILE -- Creates a new FileObject based on a Google Drive file.
   *
   * @param {GoogleAppsScript.Drive.File} file The Drive file to use.
   * @param {string} currentFolderId       The id of the folder in which the Drive file currently resides. Because in reality, each file can be in many folders, and we need to choose one.
   *
   * @returns {FileObject}                 A FileObject representation of the Drive file.
   */
  export function buildFileObjectFromFile(file: GoogleAppsScript.Drive.File): FileObject {
    let firstEditor: DriveUser = driveManipulation.getFirstEditor(file);

    // Get parent folders
    let folders: GoogleAppsScript.Drive.FolderIterator = file.getParents();
    let folderids: string[] = [];

    // Go through parent folders
    while (folders.hasNext()) {
      let folder: GoogleAppsScript.Drive.Folder = folders.next();

      // Add each parent folder Id to the array
      folderids.push(folder.getId());
    }

    return {
      name: file.getName(),
      url: file.getUrl(),
      id: file.getId(),
      currentFolderIds: folderids,
      targetFolderString: null,
      date: file.getDateCreated().toString(),
      creator: {
        displayName: firstEditor.displayName,
        emailAddress: firstEditor.emailAddress
      }
    };
  }

  /**
   * BUILD FOLDER OBJECT FROM FOLDER -- Creates a new FolderObject based on a Google Drive folder.
   *
   * @param {GoogleAppsScript.Drive.Folder} folder The Drive folder to use.
   * @param {FolderReferenceMap} existingFolders A hashmap of Drive folders, which this function helps populate.
   * @param {FileReferenceMap} existingFiles A hashmap of Drive files, which this function helps populate.
   * @param {number} depth                 The current recursion depth.
   *
   * @returns {FolderObject}               A FolderObject representing the specified Drive folder, including files and subfolders.
   */
  export function buildFolderObjectFromFolder(
    folder: GoogleAppsScript.Drive.Folder,
    existingFolders: FolderReferenceMap,
    existingFiles: FileReferenceMap,
    depth: number = 0
  ): FolderObject {
    if (depth >= server_globals.maxDepth) {
      throw "Max subfolder depth reached";
    }

    // Create base Folder object
    let folderObject: FolderObject = buildFolderObject(folder.getName());
    folderObject.url = folder.getUrl();
    folderObject.id = folder.getId();

    // Get all files from this folder
    folderObject.files = objectStructure.makeFileObjectsArray(folder, folderObject, existingFiles);

    // Get all folders, recursively, from this folder
    let subfolderIterator: GoogleAppsScript.Drive.FolderIterator = folder.getFolders();

    while (subfolderIterator.hasNext()) {
      let subfolder: GoogleAppsScript.Drive.Folder = subfolderIterator.next();
      let subfolderObject = buildFolderObjectFromFolder(subfolder, existingFolders, existingFiles, depth + 1);
      subfolderObject.id = subfolder.getId();
      folderObject.folders.push(subfolderObject);
    }

    // Add folder reference to map
    let folderId = folder.getId();
    if (!existingFolders.hasOwnProperty(folderId)) {
      existingFolders[folderId] = folder;
    }

    // Return
    return folderObject;
  }

  /**
   * BUILD FOLDER OBJECT -- Creates a new, empty FolderObject
   *
   * @param {string} folderName            The folder's name.
   *
   * @returns {FolderObject}               The new FOlderObject.
   */
  export function buildFolderObject(folderName: string): FolderObject {
    return {
      name: folderName,
      id: "",
      url: "",
      folders: [],
      files: []
    };
  }
}

/**
 * DRIVE MANIPULATION -- A collection of functions to make changes to the Drive folders/files structure
 */
namespace driveManipulation {
  /**
   * MOVE FILE -- Move file from one folder to another. Or rather, adds it to one folder while removing it from one or more (detected) folders.
   *
   * @param {FileObject} fileObject        The file object representing the Drive file to be moved.
   * @param {string} toFolderId            The id of the Drive folder to add the Drive file to.
   * @param {FolderReferenceMap} existingFolders A reference map of detected folders.
   * @param {GoogleAppsScript.Drive.File} file The Drive file, in case we already have a reference to it.
   *
   * @returns {FileObject}                 The same FileObject that was passed into the function, now with a different parent folder.
   */
  export function moveFile(
    fileObject: FileObject,
    toFolderId: string,
    existingFolders: FolderReferenceMap,
    file: GoogleAppsScript.Drive.File = null
  ): FileObject {
    // Get file reference if we don't already have it
    if (file == null) {
      file = DriveApp.getFileById(fileObject.id);
    }

    // Add file to target folder if it is not already there
    // Also make sure the given folder id actually corresponds to a detected folder.
    if (fileObject.currentFolderIds.indexOf(toFolderId) < 0 && existingFolders.hasOwnProperty(toFolderId)) {
      let toFolder: GoogleAppsScript.Drive.Folder = existingFolders[toFolderId];
      toFolder.addFile(file);
      server_globals.filesMoved++;
    }

    // Remove file from all (detected) folders except the target
    fileObject.currentFolderIds.forEach(function(currentFolderId) {
      if (currentFolderId != toFolderId && existingFolders.hasOwnProperty(currentFolderId)) {
        let fromFolder: GoogleAppsScript.Drive.Folder = existingFolders[currentFolderId];

        fromFolder.removeFile(file);
      }
    });

    // Change the file's name if it does not correspond to the name stated in the FileObject
    if (file.getName() != fileObject.name) {
      file.setName(fileObject.name);
    }

    return fileObject;
  }

  /**
   * GET FOLDER REFERENCE -- Get a Drive folder reference for a named subfolder of a given Drive folder. Create the subfolder if necessary.
   *
   * @param {string} folderName            The name of the subfolder.
   * @param {GoogleAppsScript.Drive.Folder} parentFolder The Drive folder.
   *
   * @returns {GoogleAppsScript.Drive.Folder} The Drive subfolder
   */
  export function getFolderReference(
    folderName: string,
    parentFolder: GoogleAppsScript.Drive.Folder
  ): GoogleAppsScript.Drive.Folder {
    // TODO: Rename this to something more sensible, to reflect it creates, not only gets
    // Try to find folder in parentfolder, based on name
    let foldersByTheName: GoogleAppsScript.Drive.FolderIterator;
    foldersByTheName = parentFolder.getFoldersByName(folderName);

    let folderExists: boolean = foldersByTheName.hasNext();

    // If it does not exist, it becomes necessary to create it
    if (folderExists) {
      return foldersByTheName.next();
    } else {
      server_globals.foldersCreated++;
      return parentFolder.createFolder(folderName);
    }
  }

  /**
   * GET FIRST EDITOR -- Get the "first editor" of a given Drive file
   *
   * @param {GoogleAppsScript.Drive.File} file The Drive file.
   *
   * @returns {DriveUser}                  The editor.
   */
  export function getFirstEditor(file: GoogleAppsScript.Drive.File): DriveUser {
    let revisions: DriveRevision[] = Drive.Revisions.list(file.getId()).items;

    let firstRevision: DriveRevision = revisions[revisions.length - 1];

    return firstRevision.lastModifyingUser;
  }

  /**
   * APPLY STRUCTURE TO FOLDER -- Use a structure of FolderObjects and FileObjects as a template for creating Drive folders and moving Drive files.
   *
   * @param {FolderObject} folderObject    The FolderObject to work on.
   * @param {FolderReferenceMap} existingFolders A hashmap of existing Drive folders, to use for quick access.
   * @param {FileReferenceMap} existingFiles A hashmap of existing Drive files, to use for quick access.
   * @param {GoogleAppsScript.Drive.Folder} [parentFolder=null] The this FolderObject's parent folder.
   */
  export function applyStructureToFolder(
    folderObject: FolderObject,
    existingFolders: FolderReferenceMap,
    existingFiles: FileReferenceMap,
    parentFolder: GoogleAppsScript.Drive.Folder = null
  ): void {
    // First, make sure we have a reference to "this folder"; create it if necessary
    let folder: GoogleAppsScript.Drive.Folder;
    let folderId: string;

    if (parentFolder) {
      // Is not first/root folder, so folder likely needs to be created inside parent folder
      folder = getFolderReference(folderObject.name, parentFolder);

      // Make sure the new folder is added to the hashmap of existing folders
      existingFolders[folder.getId()] = folder;
    } else {
      folder = DriveApp.getFolderById(folderObject.id);
    }

    if (folder) {
      // Move all files that should be in this folder here
      for (let i = 0; i < folderObject.files.length; i++) {
        let fileObject = folderObject.files[i];
        let file = existingFiles[fileObject.id];
        let targetFolder = folder;

        // Move the file
        moveFile(fileObject, targetFolder.getId(), existingFolders, file);

        // TODO: Check if the id really needs to be gotten
      }

      // Then, iterate through all subfolders, with this folder as parent
      for (let i = 0; i < folderObject.folders.length; i++) {
        let subFolderObject: FolderObject = folderObject.folders[i];

        applyStructureToFolder(subFolderObject, existingFolders, existingFiles, folder);
      }
    }
  }

  /**
   * TRASH FOLDER -- Trash all empty subfolders of the specified Drive folder. Empty = contains no files. Recursive.
   *
   * @param {GoogleAppsScript.Drive.Folder} folder The folder to go through.
   * @param {number} depth                 Current recursion depth.
   */
  export function TrashFolder(folder: GoogleAppsScript.Drive.Folder, depth: number = 0): boolean {
    if (depth >= server_globals.maxDepth) {
      throw "Max subfolder depth reached";
    }

    let numFolders = 0;
    let subfolders: GoogleAppsScript.Drive.FolderIterator = folder.getFolders();

    while (subfolders.hasNext()) {
      let subfolder: GoogleAppsScript.Drive.Folder = subfolders.next();

      let subfolderIsEmpty: boolean = TrashFolder(subfolder, depth + 1);

      if (subfolderIsEmpty) {
        subfolder.setTrashed(true);
        server_globals.foldersTrashed++;
      } else {
        numFolders++;
      }
    }

    // Are there any files?
    let hasFiles = folder.getFiles().hasNext();

    // Return true if there are neither folders nor files
    if (numFolders == 0 && !hasFiles) {
      return true;
    } else {
      return false;
    }
  }
}
