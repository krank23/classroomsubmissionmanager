var languageStrings = {
  en: {
    /* Longer texts */

    introtext:
      '<p>Welcome to Classroom TA! This webapp is intended to work as a "teaching assistant" for users of Google Classroom (hence the name).</p>' +
      "<p><strong>The problem</strong> it is designed to solve is this: When students submit assignments to Classroom, their files are all placed inside a single Drive folder, created by Classroom. This works well in some subjects and for many teachers, but not all. For example, it might be a hassle if students don't name their assignments corrently or if each student submits multiple files.</p>" +
      "<p><strong>The solution</strong> ClassroomTA You pick a folder to organize. Then, the current structure of the chosen folder is shown on the right, while a preview of the result is shown on the right. You may also decide on whether there should be additional subfolders created based on the date of each file's submission.</p>" +
      '<p>Then, the you simply click  "Organize" and wait while the TA does the work.</p>' +
      '<p>Currently, the app works best when students E-mail adresses are in the form name.surname@school.domain, in which case the new folders will be named using the pattern "surname.name". In the future, other naming schemes may be added.</p>',

    donemessage:
      '<p>Created %1 folders and moved %2 files in <a href="%3">this folder</a>. This operation took %4 seconds.</p>',

    organizeconfirm:
      '<p>Are you sure? "Yes" means folders will be created and files moved into them. This may take several minutes.</p>',

    flattenconfirm:
      '<p>Are you sure? "Yes" means all files in all subfolders will be moved to the base folder you selected. This may take some time.</p>',

    removeemptyconfirm:
      '<p>Are you sure? "Yes" means all subfolders that do not contain files or non-empty subfolders are trashed recursively. This may take some time.</p>',

    flattendone:
      '<p>Moved %1 files to <a href="%2">this folder</a>. This operation took %3 seconds.</p>',

    "Max subfolder depth reached": "Max subfolder depth reached",

    updatewarningtext:
      "<p>Because you made changes to the sorting options you will need to update the preview.</p>",

    /* Header strings */
    "Working as": "Working as",
    "No folder selected": "No folder selected",
    "Folder id": "Folder id",

    /* Date subfolder options */
    "Date subfolders:": "Date subfolders:",
    "If need be": "If need be",
    Always: "Always",
    Never: "Never",

    /* Buttons */
    "Choose folder": "Choose folder",
    Update: "Update",
    Organize: "Organize",
    Flatten: "Flatten",
    "Remove empty folders": "Remove empty folders",

    /* File lists */
    Before: "Current",
    After: "After organizing",
    "Folder is empty": "Folder is empty",

    /* Loading screen */
    "Wait, working…": "Wait, working…",

    /* Footer */
    "Source on GitLab": "Source on GitLab",
    "The last operation took %1 seconds": "The last operation took %1 seconds",

    /* Titles */
    Welcome: "Welcome",
    Warning: "Warning",
    Finished: "Finished",
    Message: "Message",

    /* Dialog choices */
    Yes: "Yes",
    No: "No",
    Ok: "Ok"
  },
  sv: {
    introtext:
      "<p>Välkommen till ClassroomTA! Den här webbappen är tänkt att fungera som en lärarassistent (Teacher's Assistant) till lärare som använder Google Classroom (därav namnet).</p>" +
      "<p><strong>Problemet</strong> appen är tänkt att lösa är detta: När elever gör inlämningar i Classroom så hamnar alla deras filer i en och samma Drive-mapp, som automatiskt skapats av Classroom. Det här fungerar bra i vissa ämnen och för många lärare, men inte alls. Till exempel kan det bli problem om eleverna inte döper sina inlämningar ordentligt eller om varje elev lämnar in flera filer.</p>" +
      "<p><strong>Lösningen</strong> som ClassroomTA tillhandahåller är denna: Du väljer en mapp du vill organisera. Därefter visas den nuvarande strukturen till vänster och en förhandsvisning av strukturen som kommer att skapas till höger. Du har också möjligheten att välja huruvida ytterligare undermappar ska skapas baserat på datumet varje fil lämnades in." +
      '<p>Sedan klickar du helt enkelt på "Organisera" och väntar på att lärarassistenten ska göra jobbet.</p>' +
      "<p>Just nu fungerar assistenten bäst när elevernas mailadresser har formen förnamn.efternamn@skola.domän. Då skapas de nya mapparna efter mönstret efternamn.förnamn, och sorteras därmed i bokstavsordning efter klassklistan. I framtiden kan andra metoder för att bestämma mappnamnen läggas till.</p>",

    donemessage:
      '<p>Skapade %1 mappar och flyttade %2 filer i <a href="%3">den här mappen</a>. Operationen tog %4 sekunder.',

    organizeconfirm:
      '<p>Är du säker? "Ja" innebär att mappar skapar och filer flyttas. Detta kan ta flera minuter.</p>',

    flattenconfirm:
      '<p>Är du säker? "Ja" innebär att alla filer i alla undermappar kommer att flyttas till den basmapp du valt. Det kan ta lite tid.</p>',

    removeemptyconfirm:
      '<p>Är du säker? "Ja" innebär att alla undermappar som inte innehåller filer eller icke-tomma undermappar läggs i papperskorgen, rekursivt. Det kan ta lite tid.</p>',

    removeemptydone: "<p>Tog bort %1 mappar. Operationen tog %2 sekunder.</p>",

    flattendone:
      '<p>Flyttade %1 filer till <a href="%2">den här mappen</a>. Operationen tog %3 sekunder.</p>',

    "Max subfolder depth reached": "Nådde maximalt antal undermappar",

    updatewarningtext:
      "<p>Eftersom du ändrade inställningarna behöver du uppdatera förhandsvisningen.</p>",

    /* Header strings */
    "Working as": "Arbetar som",
    "No folder selected": "Ingen mapp vald",
    "Folder id": "Folder-id",

    /* Date subfolder options */
    "Date subfolders": "Datum-undermappar",
    "If need be": "Om det behövs",
    Always: "Alltid",
    Never: "Aldrig",

    /* Buttons */
    "Choose folder": "Välj mapp",
    Update: "Uppdatera",
    Organize: "Organisera",
    Flatten: "Platta till",
    "Remove empty folders": "Ta bort tomma mappar",

    /* File lists */
    Before: "Nuvarande",
    After: "Efter organisering",
    "Folder is empty": "Mappen är tom",

    /* Loading screen */
    "Wait, working…": "Vänta, arbetar…",

    /* Footer */
    "Source on GitLab": "Källkod på GitLab",
    "The last operation took %1 seconds":
      "Den senaste operationen tog %1 sekunder",

    /* Titles */
    Welcome: "Välkommen",
    Warning: "Varning",
    Finished: "Färdig",
    Message: "Meddelande",

    /* Dialog choices */
    Yes: "ja",
    No: "Nej",
    Ok: "Ok"
  }
};

/**
 * GET I18N -- Gets the language strings of the specified language.
 *
 * @param {string} [lang]                  The locale to get strings for.
 *
 * @returns                                The language strings.
 */
export function getI18n(lang: string): any {
  if (!lang) {
    lang = Session.getActiveUserLocale();
  }

  if (!languageStrings.hasOwnProperty(lang)) {
    lang = "en";
  }

  return languageStrings[lang];
}
