/* HTML Service functions */

/**
 * DO GET -- Default HTML helper function to HTTP GET the contents of html/index
 * 
 * @returns {GoogleAppsScript.HTML.HtmlTemplate} The HTML.
 */
function doGet() {
  return HtmlService.createTemplateFromFile('html/index')
      .evaluate()
      .setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL);
}

/**
 * INCLUDE -- Default helper function to get and insert contents of a specified html file into the calling html file.
 * 
 * @param {string} filename                The html file to include.
 * 
 * @returns {string}                       The contents of the html file.
 */
function include(filename : string) {
  return HtmlService.createHtmlOutputFromFile(filename)
      .getContent();
}

/**
 * INIT PICKER -- Return the config for the folder picker on the client side.
 * 
 * @returns {PickerConfig}                 The picker config.
 */
function initPicker(): PickerConfig {
  return {
    token : ScriptApp.getOAuthToken(),
    origin : "https://script.google.com",
    developerKey : secrets.developerKey, // Secrets are defined in secrets.js, which is not git:ed for obvious reasons
  }
}

/**
 * GET CURRENT USERNAME -- Gets the current user's E-mail (google login).
 * 
 * @returns {string}                       The E-mail.
 */
function getCurrentUserName() {
  return Session.getActiveUser().getEmail();
}