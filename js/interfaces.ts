/**
 * PICKER CONFIG -- Configuration interface for a file picker.
 * 
 * @property {string} token                The token for the logged-in user.
 * @property {string} origin               The origin of the request.
 * @property {string} developerKey         The developer key.
 */
interface PickerConfig {
  token: string;
  origin: string;
  developerKey: string;
}

/**
 * BUTTON OBJECT -- A clickable button interface.
 * 
 * @property {string} text                 The text for the button
 * @function action()                      Function to be called when clicking the button
 */
interface ButtonObject {
  text: string;
  action(): void;
}

/* -----------------------------------------------------------------------------
    MAPS 
------------------------------------------------------------------------------*/

/**
 * FOLDER REFERENCE MAP
 * id(string) -> Drive folder
 */
interface FolderReferenceMap {
  [index: string]: GoogleAppsScript.Drive.Folder;
}

/**
 * FILE REFERENCE MAP
 * id(string) -> Drive file
 */
interface FileReferenceMap {
  [index: string]: GoogleAppsScript.Drive.File;
}

/**
 * BOOLEAN MAP
 * string -> boolean
 */
interface BoolMap {
  [index: string]: boolean;
}

/**
 * INT MAP
 * string -> number
 */
interface NumberMap {
  [index: string]: number
}

/**
 * STRING MAP
 * string -> string
 */
interface StringMap {
  [index: string]: string;
}

/**
 * FOLDER OBJECT MAP
 * name(string) -> FolderObject
 */
interface FolderObjectMap {
  [index: string]: FolderObject;
}

/* -----------------------------------------------------------------------------
    RESULT
------------------------------------------------------------------------------*/

/**
 * SORTING RESULT
 * @property {FolderObject} before         The "before" folder structure.
 * @property {FolderObject} after          The "after" folder structure.
 * @property {Result} result               Details and stats.
 * @property {number} result.foldersTrashed The number of folders trashed.
 * @property {number} result.foldersCreated The number of folders created.
 * @property {number} result.filesMoved    The number of files moved.
 */
interface SortingResult {
  before: FolderObject;
  after: FolderObject;
  result: Result;
}

/**
 * RESULT STATS
 * @property {number} foldersTrashed       The number of folders trashed.
 * @property {number} foldersCreated       The number of folders created.
 * @property {number} filesMoved           The number of files moved.
 */
interface Result {
  foldersTrashed: number;
  foldersCreated: number;
  filesMoved: number;
  filesRenamed: number;
}

/* -----------------------------------------------------------------------------
    FILE & FOLDER ABSTRACTIONS
------------------------------------------------------------------------------*/

/**
 * FOLDER OBJECT -- Describes a Drive folder and its contents, without having any actual connection to the Drive API.
 * 
 * @property {string} name                 The folder's name.
 * @property {string} id                   The Drive folder's id.
 * @property {string} url                  The URL of the Drive folder.
 * @property {FolderObject[]} folders      The subfolders.
 * @property {FileObject[]} files          The files.
 */
interface FolderObject {
  name: string;
  id: string;
  url: string;
  folders: FolderObject[];
  files: FileObject[];
}

/**
 * FILE OBJECT -- Describes a Drive file, without having any actual connection to the Drive API.
 * 
 * @property {string} name                 The file's name.
 * @property {string} id                   The file's Drive id.
 * @property {url} url                     The URL of the file.
 * @property {string[]} currentFolderIds   The ids of the (detected) Drive folders that the file currently resides in
 * @property {string} targetFolderString   The name of the Drive folder that the file should be moved to.
 * @property {string} date                 The date of the file's original creation, as a ECMA-262 datestring.
 * @property {CreatorObject} creator       The file's original creator.
 * @property {string} creator.displayName  The display name of the file's original creator.
 * @property {string} creator.email        The E-mail of the file's original creator.
 */
interface FileObject {
  name: string;
  id: string;
  url: string;
  currentFolderIds: string[];
  targetFolderString: string;
  date: string;
  creator: DriveUser;
}

/* -----------------------------------------------------------------------------
    DRIVE ADVANCED SERVICE INTERFACES
------------------------------------------------------------------------------*/

/**
 * DRIVE USER -- Describes the creator of a Drive item
 * 
 * @property {string} displayName          The user's display name.
 * @property {string} emailAddress         The user's E-mail.
 */
interface DriveUser {
  displayName: string;
  emailAddress: string;
}

/**
 * DRIVE REVISION -- interface describing the only part of a Drive object's revisions we are interested in.
 * 
 * @property {DriveUser} lastModifyingUser The last user to modify this revision.
 * @property {string} lastModifyingUser.displayName The user's display name.
 * @property {string} lastModifyingUser.emailAddress The user's E-mail.
 */
interface DriveRevision {
  lastModifyingUser: DriveUser;
}
