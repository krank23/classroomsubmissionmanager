/* TIMER */

/**
 * TIMER -- Allowing the global timing of events
 * 
 * @property {Date} startDate
 * @property {number} lastResult
 * @property {boolean}
 * 
 */
namespace timer {
  let startDate:Date = new Date();
  export let lastResult:number = 0;
  let running:boolean = false;

  /**
   * START -- start the timer.
   */
  export function start(): void {
    running = true;
    startDate = new Date();
  }

  /**
   * STOP -- stop the timer.
   */
  export function stop(): void {
    if (running) {
      let timerDiff: number = new Date().getTime() - startDate.getTime();
      timer.lastResult = timerDiff;
    }
  }
}
