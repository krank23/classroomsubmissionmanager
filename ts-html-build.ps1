param (
    [Parameter(Mandatory=$true)][string]$file_basename
)

# Generate file names
$ts_filename = $file_basename + ".ts"
$js_filename = $file_basename + ".js"
$html_filename = "build\" + $file_basename + ".html"

# Compile typescript
tsc --out $js_filename $ts_filename --skipLibCheck

Write-Host "HTML filename: " + $html_filename

# Get contents of resulting js file
$file_contents = Get-Content $js_filename

# Clear the file if it exists. If it does not, create it.
if (Test-Path $html_filename)
{
    # Clear the file
    Clear-Content $html_filename
}
else
{
    # Create the file
    New-Item $html_filename
}

# Add script tags + js file contents
Add-Content $html_filename "<script>`n"

Add-Content $html_filename $file_contents

Add-Content $html_filename "`n</script>"

# Delete the js intermediary
Remove-item $js_filename