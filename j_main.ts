/// <reference path="google.script.d.ts" />

/// <reference path="js/interfaces.ts" />

/// <reference path="j_modal.ts" />
/// <reference path="j_folderpicker.ts" />
/// <reference path="j_domwrangling.ts" />
/// <reference path="j_timer.ts" />
/// <reference path="j_i18n.ts" />
/// <reference path="j_elementmaker.ts" />

/// <reference path="j_responsehandling.ts" />
/// <reference path="j_requesthandling.ts" />

var global = {
  numFolders: 0,
  numFiles: 0,
  folderId: "",
  folderName: "",
  currentPreviewDatefolderOption: ""
};

var querySelectors = {
  workingPlate: ".working-plate",
  updateWarningPlate: ".update-warning",

  userName: ".user-name",
  folderName: ".folder-name",
  folderId: ".folder-id"
};

/* ON DOCUMENT LOAD */
document.addEventListener("DOMContentLoaded", function(event): void {
  // MAKE BUTTONS WORK
  document.querySelector("button.choose-folder").addEventListener("click", chooseAction);

  document.querySelector("button.update").addEventListener("click", updateAction);

  document.querySelector("button.organize").addEventListener("click", runAction);

  document.querySelector("button.flatten").addEventListener("click", flattenAction);

  document.querySelector("button.remove-empty").addEventListener("click", removeEmptyAction);

  document.querySelectorAll('input[type="radio"][name="datefolders"]').forEach(function(element: HTMLElement) {
    element.addEventListener("click", changeOption);
  });

  // Get current user's name
  let usernameRunner = googleAppsRequest(displayUser, errorMessage);
  usernameRunner.getCurrentUserName();
});

/*------------------------------------------------------------------------------
    BUTTON ACTIONS
------------------------------------------------------------------------------*/

/**
 * CHOOSE ACTION -- Open folder picker to allow user to choose a Drive folder.
 */
function chooseAction(): void {
  // Disable buttons
  setButtonActive(".controls button", false);

  // Initialize folder picker
  folderPicker.init(folderSelected);
}

/**
 * UPDATE ACTION -- Reload the selected folder, updating the preview of the Drive folder's organized version.
 */
function updateAction(): void {
  run(global.folderId, true, showFiles);
}

/**
 * RUN ACTION -- Do the actual organizing of the selected Drive folder.
 */
function runAction(): void {
  modalDialog.showDialog(_.t("Warning"), _.t("organizeconfirm"), [
    {
      text: _.t("No"),
      action: null
    },
    {
      text: _.t("Yes"),
      action: function() {
        run(global.folderId, false, organizingDoneMessage);
      }
    }
  ]);
}

/**
 * FLATTEN ACTION -- Flatten the selected Drive folder; move all files from its subfolders, recursively, to the folder itself.
 */
function flattenAction(): void {
  modalDialog.showDialog(_.t("Warning"), _.t("flattenconfirm"), [
    {
      text: _.t("No"),
      action: null
    },
    {
      text: _.t("Yes"),
      action: function() {
        flatten(global.folderId, flatteningDoneMessage);
      }
    }
  ]);
}

/**
 * REMOVE EMPTY ACTION -- Recursively remove empty subfolders of the selected Drive folder.
 */
function removeEmptyAction(): void {
  modalDialog.showDialog(_.t("Warning"), _.t("removeemptyconfirm"), [
    {
      text: _.t("No"),
      action: null
    },
    {
      text: _.t("Yes"),
      action: function() {
        removeEmpty(global.folderId, removeEmptyDoneMessage);
      }
    }
  ]);
}
