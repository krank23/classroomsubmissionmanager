/*------------------------------------------------------------------------------
    SERVER RESPONSE HANDLING
------------------------------------------------------------------------------*/

/**
 * ERROR MESSAGE - Show an error message as a result of an error from the server.
 * 
 * @param {Error} error                    An error.
 */
function errorMessage(error: Error): void {
  timer.stop();
  // Reset controls
  resetControls(false);
  // Show error, translated
  modalDialog.showSimpleDialog(_.t("Warning"),_.t(error.message));
}

/**
 * SHOW FILES -- Display the file structures returned by the server.
 * 
 * @param {SortingResult} response         The sorting result returned by the server.
 */
function showFiles(response: SortingResult): void {
  let folderTemplate: HTMLTemplateElement = document.querySelector(".folder-template");
  let fileTemplate: HTMLTemplateElement = document.querySelector(".file-template");

  // Set column headers
  _.tMultiple({
    ".column-before-title": "Before",
    ".column-after-title": "After"
  });

  // Before
  if (response.hasOwnProperty("before")) {
    let baseContainer: HTMLElement = document.querySelector(".column-before .folder-structure");
    let folderElement: DocumentFragment = elementMaker.makeFolderElement(response.before, folderTemplate, fileTemplate);
    baseContainer.appendChild(folderElement);
  }

  // After
  if (response.hasOwnProperty("after")) {
    let baseContainer: HTMLElement = document.querySelector(".column-after .folder-structure");
    let folderElement: DocumentFragment = elementMaker.makeFolderElement(response.after, folderTemplate, fileTemplate);
    baseContainer.appendChild(folderElement);
  }

  // Stop the timer if it has not already been stopped, display the results
  timer.stop();
  let seconds: number = timer.lastResult / 1000;
  let timerResultElement: HTMLElement = document.querySelector(".timer-result");
  timerResultElement.innerHTML = _.t("The last operation took %1 seconds").replace("%1", seconds.toString());

  // Reset things
  resetControls(true);
}

/**
 * ORGANIZING DONE MESSAGE -- Display a message informing the user about the results of the folders being organized.
 * 
 * @param {SortingResult} response         The sorting result returned by the server.
 */
function organizingDoneMessage(response: SortingResult): void {
  // Build message
  let seconds: number = timer.lastResult / 1000;
  let foldersCreated: number = response.result.foldersCreated;
  let filesMoved: number = response.result.filesMoved;
  let doneMessage: string = _.t("donemessage")
    .replace("%1", foldersCreated.toString())
    .replace("%2", filesMoved.toString())
    .replace("%3", response.before.url)
    .replace("%4", seconds.toString());

  // Display dialog
  modalDialog.showSimpleDialog(_.t("Finished"), doneMessage);

  // use showFiles
  showFiles(response);
}

/**
 * FLATTENING DONE MESSAGE -- Display a message informing the user about the results of the folder being flattened.
 * 
 * @param {SortingResult} response         The sorting result returned by the server.
 */
function flatteningDoneMessage(response: SortingResult): void {
  timer.stop();

  // Build message
  let seconds: number = timer.lastResult / 1000;
  let filesMoved: number = response.result.filesMoved;
  let doneMessage: string = _.t("flattendone")
    .replace("%1", filesMoved.toString())
    .replace("%2", response.before.url)
    .replace("%3", seconds.toString());

  // Display dialog
  modalDialog.showSimpleDialog(_.t("Finished"), doneMessage);

  // use showFiles
  showFiles(response);
}

/**
 * REMOVE EMPTY DONE MESSAGE -- Display a message informing the user about the results of removing empty subfolders.
 * 
 * @param {SortingResult} response         The sorting result returned by the server.
 */
function removeEmptyDoneMessage(response: SortingResult): void {
  timer.stop();

  // Build message
  let seconds: number = timer.lastResult / 1000;
  let foldersTrashed: number = response.result.foldersTrashed;
  let doneMessage: string = _.t("removeemptydone")
    .replace("%1", foldersTrashed.toString())
    .replace("%2", seconds.toString());

  // Display dialog
  modalDialog.showSimpleDialog(_.t("Finished"), doneMessage);

  // use showFiles
  showFiles(response);
}
