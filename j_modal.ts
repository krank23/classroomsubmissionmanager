/**
 * MODAL DIALOG -- helper functions for creating and displaying modal Google-style dialog windows
 */
namespace modalDialog {
  /**
   * SHOW DIALOG -- Show a dialog.
   * 
   * @param {string} title                 Title of the dialog window.
   * @param {string} text                  Text to be displayed in the dialog.
   * @param {ButtonObject[]} buttons       An array of buttons to be placed in the actions bar of the dialog.
   */
  export function showDialog(title: string, text: string, buttons: ButtonObject[] = []) {
    let dialog: HTMLDialogElement = document.querySelector(".modal-dialog");

    if (dialog) {
      let titleElement: HTMLElement = dialog.querySelector(".modal-dialog-title");
      let textElement: HTMLElement = dialog.querySelector(".modal-dialog-content");

      if (titleElement) titleElement.innerHTML = title;
      if (textElement) textElement.innerHTML = text;

      let buttonContainer: HTMLElement = dialog.querySelector(".modal-dialog-actions");

      if (buttonContainer) {
        // Remove all old buttons
        empty(".modal-dialog-actions");

        // Go through the new buttons in reverse order
        buttons = buttons.reverse();
        for (let i = 0; i < buttons.length; i++) {
          let buttonObj: ButtonObject = buttons[i];

          // Create new button
          let buttonTemplate: HTMLTemplateElement = document.querySelector(".modal-dialog-button");
          let buttonElement: DocumentFragment = document.importNode(buttonTemplate.content, true);

          let buttonElementInner: HTMLElement = buttonElement.querySelector("button");

          // Add button text
          let buttonText: string = buttonObj.hasOwnProperty("text") ? buttonObj.text : "x";
          buttonElementInner.innerHTML = buttonText;

          // Add button event listener
          buttonElementInner.addEventListener("click", function() {
            if (buttonObj.action) {
              buttonObj.action();
            }
            dialog.close();
          });

          // Add button to container
          buttonContainer.appendChild(buttonElement);
        }
      }
      dialog.showModal();
    }
  }

  /**
   * SHOW SIMPLE DIALOG -- Show a simple dialog with just a single "OK" button that closes the dialog.
   * 
   * @param {string} title                 Title of the dialog window.
   * @param {string} text                  Text to be displayed in the dialog.
   */
  export function showSimpleDialog(title: string, text: string): void {
    showDialog(_.t(title), text, [
      {
        text: _.t("Ok"),
        action: null
      }
    ]);
  }
}
