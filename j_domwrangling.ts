/*------------------------------------------------------------------------------
    GENERAL DOM WRANGLING
------------------------------------------------------------------------------*/

/**
 * EMPTY -- Empty all elements matching a selector
 *
 * @param {string} elementSelector         The selector to match
 */
function empty(elementSelector: string): void {
  let elements: NodeListOf<Element> = document.querySelectorAll(elementSelector);

  for (let i = 0; i < elements.length; i++) {
    let element = elements[i];
    element.innerHTML = "";
    forceRedraw();
  }
}

/**
 * FORCE REDRAW -- Force redraw of the HTML. Necessary to make layout follow actual changes.
 */
function forceRedraw(): void {
  let disp: string = document.body.style.display;
  document.body.style.display = "none";
  let trick: number = document.body.offsetHeight;
  document.body.style.display = disp;
}

/**
 * GET RADIO BUTTON CHOICE -- get the current choice of a radio button set, identified by name
 *
 * @param {string} nameIdentifier          The name of the radio button set.
 */
function getRadioButtonChoice(nameIdentifier: string) {
  let element: HTMLInputElement = document.querySelector("input[name='" + nameIdentifier + "']:checked");
  if (element) {
    return element.value;
  } else {
    return "";
  }
}

/*------------------------------------------------------------------------------
    SPECIFIC MANIPULATION
------------------------------------------------------------------------------*/

/**
 * DISPLAY USER -- Insert the user's name/identifier into the correct HTML element.
 *
 * @param {string} user                    The user's name/identifier
 */
function displayUser(user: string): void {
  document.querySelector(".user-name").innerHTML = user;
}

/**
 * SET TITLE -- Insert the folder's name and ID into the correct HTML element.
 *
 * @param {string} folderName              The folder's name.
 * @param {string} folderId                The folder's id.
 */
function setFolderDisplay(folderName: string, folderId: string): void {
  global.folderName = folderName;
  global.folderId = folderId;

  document.querySelector(querySelectors.folderName).innerHTML = folderName;
  document.querySelector(querySelectors.folderId).innerHTML = folderId;
}

/**
 * RESET CONTROLS -- Reset the status of the various buttons, their state being determined by whether a folder has been selected.
 *
 * @param {boolean} folderHasBeenSelected  Whether a folder has been selected.
 */
function resetControls(folderHasBeenSelected: boolean): void {
  setPlateStatus(querySelectors.workingPlate, false);

  if (folderHasBeenSelected) {
    setButtonActive(".controls button", true);
    setButtonActive(".column-before-actions button", true);
  } else {
    setButtonActive(".controls button", false);
    setButtonActive(".controls button.choose-folder", true);
    setButtonActive(".column-before-actions button", false);
    setFolderDisplay(_.t("No folder selected"), "");
  }
}

/**
 * SET BUTTON ACTIVE -- Set whether a specific button set - identified by an element selector - should be set as active.
 *
 * @param {string} elementSelector         The element selector identifying the button set.
 * @param {boolean} active                 Whether the button set should be sat as active or not.
 */
function setButtonActive(elementSelector: string, active: boolean): void {
  let elements: NodeListOf<Element> = document.querySelectorAll(elementSelector);

  for (let i = 0; i < elements.length; i++) {
    let element: Element = elements[i];
    if (active) {
      element.removeAttribute("disabled");
    } else {
      element.setAttribute("disabled", "");
    }
  }
}

/**
 * SET PLATE STATUS -- Set whether or not a plate or panel should display: none or display: flex.
 *
 * @param {string} querySelector           The query string describing the plate or panel
 * @param {boolean} active                 Whether to set it to be active or not.
 */
function setPlateStatus(querySelector: string, active: boolean) {
  let plate: HTMLElement = document.querySelector(querySelector);

  if (active) {
    plate.style.display = "flex";
  } else {
    plate.style.display = "none";
  }
}

/**
 * CHANGE OPTION -- Called when options are changed
 */
function changeOption(): void {
  // If there is a preview...
  if (global.currentPreviewDatefolderOption != "") {
    // Get the current value of the Datefolder option
    let newDateFolderOption = getRadioButtonChoice("datefolders");

    setPlateStatus(querySelectors.updateWarningPlate, newDateFolderOption != global.currentPreviewDatefolderOption);
  }
}
