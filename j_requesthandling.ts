/*------------------------------------------------------------------------------
    SERVER REQUEST HANDLING
------------------------------------------------------------------------------*/

/**
 * GOOGLE APPS REQUEST -- Prepare a Google Apps request.
 *
 * @param {Function} successHandler        The callback for a successful request. The returned object is passed to the function as the first argument.
 * @param {Function} failureHandler        The callback for an unsuccessful request. The Error object is passed to the function as the first argument.
 * @param userObject                       The user object to be passed along with the request.
 *
 * @returns                                The runner object.
 */
function googleAppsRequest(
  successHandler: (result: any) => void,
  failureHandler: (error: Error) => void,
  userObject: Object = null
): any {
  let runner = <any>google.script.run.withFailureHandler(failureHandler);

  runner = runner.withSuccessHandler(successHandler);

  if (userObject) {
    runner = runner.withUserObject(userObject);
  }

  return runner;
}

/**
 * FOLDER SELECTED -- React to a folder having been selected by the Picker dialog.
 *
 * @param data                             The object returned by the Picker.
 */
function folderSelected(data: any): void {
  let action: string = data[google.picker.Response.ACTION];

  if (action == google.picker.Action.PICKED) {
    if (data[google.picker.Response.DOCUMENTS].length > 0) {
      let folderName: string = data[google.picker.Response.DOCUMENTS][0][google.picker.Document.NAME];
      let folderId: string = data[google.picker.Response.DOCUMENTS][0][google.picker.Document.ID];

      setFolderDisplay(folderName, folderId);

      run(folderId, true, showFiles);
    }
  } else if (action == google.picker.Action.CANCEL) {
    // Enable all buttons if a folderId was previously set
    resetControls(global.folderId != "");
  }
}

/**
 * RUN -- Organize the selected folder, or at least prepare and preview (dry-run) the organizing.
 *
 * @param {string} folderId                The Drive id of the folder to be organized.
 * @param {boolean} dryRun                 Whether to actually make changes to the Drive folder or not.
 * @param {Function} callback              The function to run upon successful completion of the operation.
 */
function run(folderId: string, dryRun: boolean, callback: (result: any) => void): void {
  processStart();

  global.currentPreviewDatefolderOption = getRadioButtonChoice("datefolders");

  let runner: any = googleAppsRequest(callback, errorMessage);
  runner.sortFilesIntoFolders(folderId, dryRun, global.currentPreviewDatefolderOption);
}

/**
 * FLATTEN -- Move all the files contained within subfolders to the parent Drive folder, "flattening" it.
 *
 * @param {string} folderId                The Drive id of the folder to be flattened.
 * @param {Function} callback              The function to run upon successful completion of the operation.
 */
function flatten(folderId: string, callback: (result: any) => void): void {
  processStart();

  global.currentPreviewDatefolderOption = getRadioButtonChoice("datefolders");

  let runner: any = googleAppsRequest(callback, errorMessage);
  runner.flattenFolder(folderId, global.currentPreviewDatefolderOption);
}

/**
 * REMOVE EMPTY -- Remove empty subfolders of specified Drive folder, recursively.
 * 
 * @param {string} folderId                The Drive id of the folder to be rid of empty subfolders.
 * @param {Function} callback              The function to run upon successful completion of the operation.
 */
function removeEmpty(folderId: string, callback: (result: any) => void) {
  processStart();

  global.currentPreviewDatefolderOption = getRadioButtonChoice("datefolders");

  let runner: any = googleAppsRequest(callback, errorMessage);
  runner.removeEmptySubfolders(folderId, global.currentPreviewDatefolderOption);
}

/**
 * PROCESS START -- Make the UI changes that indicate a process has been started, and start the timer.
 */
function processStart(): void {
  empty(".folder-structure");
  empty(".welcome-title");
  empty(".timer-result");

  setButtonActive(".controls button", false);
  setButtonActive(".column-before-actions button", false);

  setPlateStatus(querySelectors.workingPlate, true);
  setPlateStatus(querySelectors.updateWarningPlate, false);
  timer.start();
}