/*------------------------------------------------------------------------------
    TRANSLATION THINGS
------------------------------------------------------------------------------*/

document.addEventListener("DOMContentLoaded", function(event): void {
  // Get i18n stuff
  let i18nRunner = googleAppsRequest(initialTranslations, errorMessage);
  i18nRunner.getI18n();
  
});

/**
 * _ -- A collection of variables and functions for internationalization
  */
namespace _ {
  // Dictionary of translations where key = original and value = translation
  let strings: StringMap = {};

  /**
   * SET DICTIONARY -- Set the dictionary to use
   * 
   * @param {StringMap} stringDictionary 
   */
  export function setDictionary(stringDictionary: StringMap):void {
    strings = stringDictionary;
  }

  /**
   * T -- Translate a single string
   * 
   * @param {string} original              Original string; usually a short word in english or a keyword for longer texts.
   * 
   * @return {string}                      The translation, if available.
   */
  export function t(original: string): string {
    if (strings.hasOwnProperty(original)) {
      // if string is translateable, return translation
      return strings[original];
    } else {
      // if string is not in dictionarym return it unchanged
      return original;
    }
  }

  /**
   * TMULTIPLE -- Translate multiple strings and put each translated string into multiple html elements.
   * 
   * @param {StringMap} originals          A StringMap where the keys are strings to translate, value is the element selector for the elements where the key's translation is to be inserted.
   */
  export function tMultiple(originals: StringMap): void {
    // Loop through all pairs
    for (let selector in originals) {
      // Get translation
      let translation: string = t(originals[selector]);

      // Get elements
      let elements: NodeListOf<HTMLElement> = document.querySelectorAll(selector);

      // Loop through all elements, changing contents to translated string
      elements.forEach(function(element: Element) {
        element.innerHTML = translation;
      });
    }
  }
};

/**
 * INITIAL TRANSLATIONS -- Do some initial string translation.
 * 
 * @param {StringMap} stringDictionary     The dictionary to use.
 */
function initialTranslations(stringDictionary: StringMap): void {
  _.setDictionary(stringDictionary)

  // Do initial translations
  _.tMultiple({
    ".column-before-title": "Welcome",
    ".column-after-title": "",
    ".welcome-box": "introtext",
    ".label-working-as": "Working as",
    ".label-folder-id": "Folder id",
    ".label_datefolders": "Date subfolders",
    "label[for=needbe] span.mdl-radio__label": "If need be",
    "label[for=always] span.mdl-radio__label": "Always",
    "label[for=never] span.mdl-radio__label": "Never",
    ".label-choose-folder": "Choose folder",
    ".label-update": "Update",
    ".label-organize": "Organize",
    ".label-flatten": "Flatten",
    ".label-remove-empty": "Remove empty folders",
    ".working-plate__title": "Wait, working…",
    ".update-warning__title": "Update",
    ".update-warning__text": "updatewarningtext",
    ".gitlab-source": "Source on GitLab"
  });

  // Set initial title & folder id
  resetControls(false);
}