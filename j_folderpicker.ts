/**
 * FOLDER PICKER -- A collection of helper functions for showing a folder picker.
 */
namespace folderPicker {
  /**
   * INIT -- Initialize the picker, showing it when initialization is done.
   * 
   * @param {Function} callback            The function to run when the picker, once shown, has also been closed again.
   */
  export function init(callback: (result: any) => void): void {
    let runner = googleAppsRequest(<any>showPicker, errorMessage, callback);
    runner.initPicker();
  }

  /**
   * SHOW PICKER -- Display the folder picker.
   * 
   * @param {PickerConfig} config          The config received from the server, including API key and current logged-in user
   * @param {Function} callback            The function to run when the picker is closed.
   */
  export function showPicker(config: PickerConfig, callback: (result: any) => void): void {
    let view = new google.picker.DocsView(google.picker.ViewId.FOLDERS);

    view.setOwnedByMe(true);
    view.setSelectFolderEnabled(true);

    let picker = new google.picker.PickerBuilder();
    picker
      .setOAuthToken(config.token)
      .setDeveloperKey(config.developerKey)
      .setOrigin(config.origin)
      .setCallback(callback)
      .addView(view)
      .build()
      .setVisible(true);
  }
}
