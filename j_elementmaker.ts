/**
 * ELEMENT MAKER -- A collection of functions for creating HTML Elements (DocumentFragments).
 */
namespace elementMaker {
  // Function for making folder HTML elements based on data + templates. Recursively.
  
  /**
   * MAKE FOLDER ELEMENT -- Creates folder HTML elements, containing files and subfolders, recursively, based on a given FolderObject structure.
   * 
   * @param {FolderObject} folderObject    The base FolderObject, containing files and subfolders.
   * @param {HTMLTemplateElement} folderTemplate The template to use for creating the folder html elements.
   * @param {HTMLTemplateElement} fileTemplate The template to use for creating the file html elements.
   * 
   * @returns {DocumentFragment}           The resulting HTML element / DocumentFragment.
   */
  export function makeFolderElement(
    folderObject: FolderObject,
    folderTemplate: HTMLTemplateElement,
    fileTemplate: HTMLTemplateElement
  ): DocumentFragment {
    // Create folder element from template
    let newFolderElement: DocumentFragment = document.importNode(folderTemplate.content, true);

    // Set folder title & link
    let headerElement: HTMLElement = newFolderElement.querySelector(".folder-title");
    if (headerElement) {
      headerElement.innerHTML = folderObject.name;

      if (folderObject.url) {
        headerElement.setAttribute("href", folderObject.url);
      }
    }

    // Get folder's item container
    let listContainer: HTMLElement = newFolderElement.querySelector("ul");

    if (folderObject.files.length == 0 && folderObject.folders.length == 0) {
      // If there are no folders or files, add a placeholder "this is empty"
      let fileElement: DocumentFragment = makeEmptyFile(fileTemplate);
      listContainer.appendChild(fileElement);
    } else {
      // Sort subfolders by name
      folderObject.folders.sort(function(a, b): number {
        let nameA = a.name,
          nameB = b.name;
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });

      // Go through all the folder's subfolders; add them as folder elements (recursively)
      for (let i = 0; i < folderObject.folders.length; i++) {
        let subFolderObject: FolderObject = folderObject.folders[i];

        let subFolderElement: DocumentFragment = makeFolderElement(subFolderObject, folderTemplate, fileTemplate);

        listContainer.appendChild(subFolderElement);
      }

      // Sort files by name
      folderObject.files.sort(function(a:FileObject, b:FileObject): number {
        let nameA:string = a.name,
          nameB:string = b.name;
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });

      // Go through all the folder's files; add them as file elements
      for (let i = 0; i < folderObject.files.length; i++) {
        let fileObject: FileObject = folderObject.files[i];

        let fileElement: DocumentFragment = makeFileElement(fileObject, fileTemplate, true);

        listContainer.appendChild(fileElement);
      }
    }

    // Return the new folder element
    return newFolderElement;
  }

  /**
   * MAKE FILE ELEMENT -- Create a HTML element based on a FileObject.
   * 
   * @param {FileObject} fileObject        The file object to use as basis for the HTML element.
   * @param {HTMLTemplateElement} fileTemplate The template to use for creating the file html element.
   * @param {boolean} useTooltip           Whether to use a tooltip for the file html element.
   * 
   * @returns {DocumentFragment}           The resulting HTML element.
   */
  export function makeFileElement(
    fileObject: FileObject,
    fileTemplate: HTMLTemplateElement,
    useTooltip: boolean = true
  ): DocumentFragment {
    // Create file element from template
    let fileElement: DocumentFragment = document.importNode(fileTemplate.content, true);

    // Find & fill name
    let fileNameElement: HTMLElement = fileElement.querySelector(".file-title");
    if (fileNameElement) fileNameElement.innerHTML = fileObject.name;

    // Find & fill tooltip, if relevant
    let tooltipElement: HTMLElement = fileElement.querySelector(".file-tooltip");

    if (useTooltip) {
      let fileDate: Date = new Date(fileObject.date);

      if (tooltipElement) {
        tooltipElement.innerHTML =
          fileObject.creator.displayName +
          " (" +
          fileObject.creator.emailAddress +
          ") " +
          fileDate.toISOString().split("T")[0];
      } else {
        tooltipElement.remove();
      }
    }

    return fileElement;
  }

  /**
   * MAKE EMPTY FILE -- Create a "Folder is empty" file element.
   * 
   * @param {HTMLTemplateElement} fileTemplate The file object to use as basis for the HTML element.
   * 
   * @returns {DocumentFragment}           The resulting HTML element.
   */
  function makeEmptyFile(fileTemplate: HTMLTemplateElement): DocumentFragment {
    // Create a dummy file object and pass it to makeFileElement
    let fileElement: DocumentFragment = makeFileElement(
      {
        name: "[" + _.t("Folder is empty") + "]",
        id: null,
        url: null,
        currentFolderIds: [],
        targetFolderString: "",
        date: null,
        creator: null
      },
      fileTemplate,
      false
    );

    return fileElement;
  }
}
